FROM python:3.11.0-slim-bullseye

COPY . /app

RUN pip install -r /app/requirements.txt

EXPOSE 8000

WORKDIR /app

CMD gunicorn -w4 --bind 0.0.0.0:8000 app:app
