# SERVER CONFIGURATION
MC_SERVER_ADDRESS = '127.0.0.1'
MC_SERVER_PORT = '25565'
MC_SERVER_QUERY_PORT = '25566'
MC_DYNMAP_ADDRESS = False

# STRINGS
STRINGS = {
    "SITE_TITLE": "MC Server Status",
    "SERVER_UP": "The Minecraft Server is UP!",
    "SERVER_DOWN": "Uh-oh! Looks like the server is down. Hopefully it will be back up soon...",
    "SERVER_DOWN_IMG": "Here's a picture of my doggie in the meantime to hold you over",
    "QUERY_DOWN": "The request to query for players timed out! Please ensure the query server is accessible.",
    "NO_PLAYERS": "There's no one online, so you should get on!",
    "PLAYER_AMOUNT": "player(s) currently online:",
}
