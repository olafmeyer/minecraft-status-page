# Minecraft Status Page

Minecraft Status Page is a creatively named web app designed to show the current status and online players for a Minecraft server.

## Installation

You can use pip to install requirements.
```bash
pip install -r requirements.txt
```
I usually like to install in a [virtual environment](https://docs.python.org/3/tutorial/venv.html).

Run a dev server with
```bash
export FLASK_APP=app.py
export FLASK_ENV=development
flask run
```

You should not use the dev server in a production environment. You will need a web server frontend like Nginx or Apache to serve it. Some guides on this are linked below, but I am sure there are many other good ones online.
- [Nginx](https://medium.com/faun/deploy-flask-app-with-nginx-using-gunicorn-7fda4f50066a)
- [Apache](https://flask.palletsprojects.com/en/1.1.x/deploying/mod_wsgi/)

## Configuration

You can customize many areas of the site by editing settings.py. The SERVER CONFIGURATION block has to do with where your webapp will look for information about the Minecraft server. MC_SERVER_ADDRESS and MC_SERVER_PORT should be pretty self-explanatory. MC_SERVER_QUERY_PORT will need to reflect the `query.port` variable in your Minecraft Server's server.properties file. I like to set this to a different port than `server-port` variable, because I'm not sure of the implications of allowing unrestricted access to the query server. Note that you will also need `enable-query` set to `true` in your server.properties file. If you are running the [Dynmap](https://dev.bukkit.org/projects/dynmap) plugin, you can add a link to that map by setting MC_DYNMAP_ADDRESS to the URL of your dynmap (usually is http://your.server.ip:8123).

The STRINGS dictionary aims to contain every string displayed on the site. I wanted it to be easy for anyone to make their webapp personal, and also to provide an easier way to do localization.

You can provide your own image for when the server is down, by replacing `static/server_down.png` with your own image, taking care to use the same name of course. You are very free to use the included image of my silly dog though, he wants to be internet famous :) 
