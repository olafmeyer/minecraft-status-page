from flask import Flask, render_template, url_for
from mcstatus import MinecraftServer

app = Flask(__name__)
app.config.from_object('settings')

@app.route('/')
def hello_minecraft_world():
 
    mc_server_addr = app.config['MC_SERVER_ADDRESS']
    mc_server_port = app.config['MC_SERVER_PORT']
    mc_server_query_port = app.config['MC_SERVER_QUERY_PORT']
    mc_dynmap_address = app.config['MC_DYNMAP_ADDRESS']
    strings = app.config['STRINGS']
   
    # use status first because it provides faster connection error than query 
    status_server = MinecraftServer.lookup(f'{mc_server_addr}:{mc_server_port}')
   
    try:
        status_server.status()
    except ConnectionRefusedError:
        return render_template('index.html', status=False, query=False, strings=strings)

    try:
        query_server = MinecraftServer.lookup(f'{mc_server_addr}:{mc_server_query_port}')
        query = query_server.query()
    except TimeoutError:
        return render_template('index.html', status=True, query=False, strings=strings)

    num_players = query.players.online
    online_players = query.players.names
    return render_template('index.html', status=True, query=True, strings=strings,
                            mc_dynmap_address=mc_dynmap_address, num_players=num_players,
                            online_players=online_players)

